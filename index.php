<?php

require_once "banco.php";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css" />
</head>
<body>
<br />
	<form action="index.php" method="POST" name="form_pesq">

		<center><div class="row"><input style="width: 40%;" class="form-control" type="text" name="pesquisa" /><input type="submit" name="sub_pesquisa" value="Pesquisar" class="btn btn-default" /></div></center>
	
	</form>

<button class="btn btn-default btn-lg"><a href="formulario.php">Cadastro</a></button>	

<br /><br /><br />
	<?php
	
		if(!isset($_POST["sub_pesquisa"])) {
			$query = "SELECT p.*,t.nome as tipo_da_pessoa FROM tbl_pessoa AS p JOIN tbl_pessoa_tipo AS t ON t.pessoa_tipo=p.empresa LIMIT 100";
		} else {		
			$query = "SELECT p.*,t.nome as tipo_da_pessoa FROM tbl_pessoa AS p JOIN tbl_pessoa_tipo AS t ON t.pessoa_tipo=p.empresa WHERE p.nome LIKE '%".$_POST["pesquisa"]."%' LIMIT 100";
		}
		$dados = pg_query($query);
	?>	
	<table class="table table-hover">
		<tr style="font-weight: bolder;font-size: 15pt;">
			<td>ID</td><td>Grupo da pessoa</td><td>Nome</td><td>Acão</td>
		</tr>
	<?php
		while ($linha = pg_fetch_array($dados)) {
			
			echo "<tr><td>".$linha["pessoa"]."</td>";
			echo "<td>".$linha["tipo_da_pessoa"]."</td>";
			echo "<td>".$linha["nome"]."</td>";		
	?>
	<div class="row">	
		<td><form action="formulario.php" name="form_editar" method="POST">
		
			<input type="hidden" name="id" value="<?= $linha["pessoa"] ?>" />
			<input type="hidden" name="empresa" value="<?= $linha["tipo_da_pessoa"] ?>" />
			<input type="hidden" name="id_empresa" value="<?= $linha["pessoa_tipo"] ?>" />
			<input type="hidden" name="nome" value="<?= $linha["nome"] ?>" />
			
			<input class="btn btn-primary col-md-5" type="submit" name="editar" value="Editar" />
		</form>
		<form action="index.php" name="form_excluir" method="POST">

			<input type="hidden" name="id" value="<?= $linha["pessoa"] ?>" />
			<input class="btn btn-danger col-md-5" type="submit" name="excluir" value="Excluir" />
			
		</form></td>
	</div>	
	<?php		
			echo "</tr>";
		}
		
		if (isset($_POST["excluir"])) {
			$id = $_POST["id"];			
			pg_query("DELETE FROM tbl_pessoa WHERE pessoa = '{$id}'");
			echo "<script>alert('Deletado com sucesso');</script>";
		}
	?>
	</table>
</body>
</html>
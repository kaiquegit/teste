<?php

require_once "banco.php";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css" />
</head>
<body>
<?php
	
	$nome = "";
	$empresa = "";
	$id = "";
	$id_empresa = "";

	if (isset($_POST["editar"])) {	
		$nome = $_POST["nome"];
		$empresa = $_POST["empresa"];
		$id = $_POST["id"];
		$id_empresa = $_POST["id_empresa"];
	}
?>
<a href="index.php">Voltar</a>
<br />
<form action="formulario.php" name="form" method="POST">
	
	<input type="hidden" name="id" value="<?= $id ?>" />
	
	&nbsp;&nbsp;&nbsp;&nbsp;Nome da pessoa	
	<input style="width: 30%;position: relative;left: 20px;" value="<?= $nome ?>" class="form-control" type="text" name="nome" required />
	<br />
	&nbsp;&nbsp;&nbsp;&nbsp;Empresa
	<select style="width: 30%;position: relative;left: 20px;" name="pessoa_tipo" class="form-control">
	<?php
	if (isset($_POST["editar"])) {
		echo '<option value="'.$id_empresa.'">'.$empresa.'</option>';
	}	
		$query = "SELECT * FROM tbl_pessoa_tipo";
		$dados = pg_query($query);
		while ($linha = pg_fetch_array($dados)) {
			echo "<option value='".$linha["pessoa_tipo"]."'>".$linha["nome"]."</option>";
		}
	?>	
	</select>
	<br />
	<?php
		if (isset($_POST["editar"])) {
	?>
		&nbsp;&nbsp;&nbsp;<input type="submit" name="submit_form" value="Editar" class="btn btn-primary" />
	<?php
	} else {
	?>
		&nbsp;&nbsp;&nbsp;<input type="submit" name="submit_form" value="Cadastrar" class="btn btn-primary" />
	<?php	
	}
	?>
</form>
<?php

if (isset($_POST["submit_form"]) && $_POST["submit_form"] == "Cadastrar") {
	
	$nome = $_POST["nome"];
	$id_tipo = $_POST["pessoa_tipo"];
	pg_query("INSERT INTO tbl_pessoa (NOME,PESSOA_TIPO,EMPRESA) values ('{$nome}','{$id_tipo}',1)");
	echo "<script>alert('Cadastrado com sucesso');</script>";
} else if (isset($_POST["submit_form"]) && $_POST["submit_form"] == "Editar") {
	$id = $_POST["id"];
	$nome = $_POST["nome"];
	$id_empresa = $_POST["empresa"];
	
	pg_query("UPDATE tbl_pessoa SET nome = '{$nome}', empresa = '{$id_empresa}' WHERE pessoa = '{$id}'");
	echo "<script>alert('Alterado com sucesso');</script>";
}

?>